import { io } from 'socket.io-client';

/**
 * @template {T extends (socket, ...args: any) => any} T
 * @typedef {T extends (socket, ...args: infer P) => any ? P : never} DropFirst
 */

const SocketService = {};


/** @type {ReturnType<typeof io>} */
let socket;

/** @type {Map<string, Array<() => void>>} */
const mapCbOn = new Map();

const getRandomHash = () => (Math.random() + 1).toString(36).substring(2);

/**
 * @template {keyof typeof import('../../../../server/index.js').EventsModule} K
 * @param {K} event
 * @param  {DropFirst<typeof import('../../../../server/index.js').EventsModule[K]>} data
 * @returns {Promise<ReturnType<typeof import('../../../../server/index.js').EventsModule[K]>>}
 */
const sendSocketEvent = (event, ...data) => {
	const hash = getRandomHash();
	const promiseOut = new Promise((res, rej) => {
		socket.once(hash, outData => {
			if (outData.code === 200) {
				res(outData.data);
			} else {
				rej(outData.message);
			}
		});
	});
	socket.send(event, hash, ...data);
	return promiseOut;
};

/**
 * @returns {Promise<boolean>}
 */
SocketService.connect = async () => {
	if (socket) {
		return;
	}
	socket = io('ws://localhost:3001');

	return new Promise(res => socket.on('connect', res))
		.then(() => {
			socket.onAny((event, ...args) => {
				if (mapCbOn.has(event)) {
					mapCbOn.get(event).forEach(cb => cb(...args));
				}
			});
			console.log('connected');
			return true;
		});
};

/**
 * @param {string} name
 */
SocketService.register = name => sendSocketEvent('register', name);

/**
 * @param {string} name
 */
SocketService.hasUserExist = name => sendSocketEvent('player_exist', name);

SocketService.sendToUser = (username, event, ...args) => sendSocketEvent('send_to_player', username, event, ...args);

SocketService.sendToAll = (event, ...args) => sendSocketEvent('broadcast', event, ...args);

/**
 * @template {keyof typeof import('../../../../server/index.js').OnEventsModule} K
 * @template {typeof import('../../../../server/index.js').OnEventsModule} T
 * @param {K} event
 * @param {(out: T[K]) => void} callback
 */
SocketService.subscribeOnEvent = (event, callback) => {
	if (mapCbOn.has(event)) {
		mapCbOn.get(event).push(callback);
	} else {
		mapCbOn.set(event, [callback]);
	}
};

/**
 * @template {keyof typeof import('../../../../server/index.js').OnEventsModule} K
 * @template {typeof import('../../../../server/index.js').OnEventsModule} T
 * @param {K} event
 * @param {(out: T[K]) => void} callback
 */
SocketService.unsubscribeOnEvent = (event, callback) => {
	if (mapCbOn.has(event)) {
		const index = mapCbOn.get(event).indexOf(callback);
		if (~index) {
			mapCbOn.get(event).splice(index, 1);
		}
	}
};

SocketService.subscribeOnEvent('user_disconnect', out => {
	console.log('Utilisateur déconnecté', out.name);
});

export default SocketService;