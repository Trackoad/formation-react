import { useCallback, useEffect, useRef, useState } from 'react';
import SocketService from '../services/socket.service';

export const FormationComponent = () => {
	const [backState, setBackState] = useState();
	const refInput = useRef();
	const onClick = useCallback(async () => {
		const out = await SocketService.register(refInput.current.value);
		setBackState(JSON.stringify(out));
	}, [refInput.current && refInput.current.value]);

	useEffect(() => {
		const receiveMessage = (message) => {
			console.log(message);
		};

		SocketService.subscribeOnEvent('chat_message', receiveMessage);

		return () => {
			SocketService.unsubscribeOnEvent(receiveMessage);
		};
	}, []);

	const onSendMessage = useCallback(() => {
		SocketService.sendToUser('melon', 'chat_message', 'coucou');
	}, []);

	return <div>
		Poker
		<input ref={refInput} />
		<button onClick={onClick}>Envoyer</button>
		{backState && <button onClick={onSendMessage}>Envoyer à {`l'utilisateur 'melon'`}</button>}
	</div>;
};