import { Server } from 'socket.io';

const io = new Server({
	cors: {
		origin: '*'
	}
});

export const EventsModule = {};

export const OnEventsModule = {};

OnEventsModule.change_name = { name: '', oldName: '' };
OnEventsModule.user_disconnect = { name: '' };

/** @type {Map<string, string>} */
const mapUsersSocketId = new Map();

/** @type {Map<string, string>} */
const mapSocketIdUser = new Map();

EventsModule.register = (socket, name) => {
	const forAll = { name };
	if (mapSocketIdUser.has(socket.id)) {
		forAll.oldName = mapSocketIdUser.get(socket.id);
		if (forAll.oldName === name) {
			return forAll;
		}
	}
	EventsModule.broadcast(socket, 'change_name', forAll);
	mapUsersSocketId.set(name, socket.id);
	mapSocketIdUser.set(socket.id, name);
	return forAll;
};

EventsModule.send_to_player = (_, targetName, event, ...data) => {
	const targetSocketId = mapUsersSocketId.get(targetName);
	if (targetSocketId) {
		io.sockets.to(targetSocketId).emit(event, ...data);
	}
};

EventsModule.player_exist = (_, targetName) => mapUsersSocketId.has(targetName);

EventsModule.broadcast = (socket, event, ...data) => {

	const rooms = [];

	io.sockets.sockets.forEach(childSocket => {
		if (!Array.from(childSocket.rooms.values()).includes(socket.id)) {
			rooms.push(childSocket.id);
		}
	});

	io.sockets.to(rooms).emit(event, ...data);
};

io.on('connection', (socket) => {
	socket.join(socket.id);
	socket.on('message', async (event, hash, ...data) => {
		const cb = EventsModule[event];

		let out = {
			code: 400,
			message: `Event not found : ${event}`,
		};
		if (cb) {
			out.data = await cb(socket, ...data);
			out.code = 200;
			delete out.message;
		}
		socket.emit(hash, out);
	});
	socket.on('disconnect', () => {
		if (mapSocketIdUser.has(socket.id)) {
			const name = mapSocketIdUser.get(socket.id);
			EventsModule.broadcast(socket, 'user_disconnect', { name });
			mapSocketIdUser.delete(socket.id);
			mapUsersSocketId.delete(name);
		}
	});
});

io.listen(3001);